# CHANGELOG

## 3.4.0
Features:
- Add flush API
- Convert and send smart tag output to obsly tags
- Smart tags: send `tag_tech_rule_id` & `tag_tech`
## 3.3.1
Fixes:
- Fix tracking upload task from file.
## 3.3.0

Features:
- Added background invalidation for performance metrics.
- Improve performance events time tracking & accuracy.

## 3.2.0

Features:
- Added `crash-metric` feature with automatic ID generation for events.
- Support for storing codable keys.
- Automatic symbols uploader.

## 3.1.1

Enhancements:
- Allowed metric completion on alternative `UIViewController`.

## 3.1.0

Features:
- New API metric API & event.

## 3.0.6

Features:
- Integrated code signing for app build and distribution security.

## 3.0.5

Fixes:
- Applied safeguards for NaN or infinite size views in UI.

## 3.0.4

Features:
- Add `PrivacyInfo.xcprivacy`.

Breacking changes:
- iOS minimum target changed to 14.1.