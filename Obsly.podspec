Pod::Spec.new do |s|
  s.name     = 'Obsly'
  s.version  = '3.4.0'
  s.ios.deployment_target     = '14.1'
  s.license  = { :type => 'Commercial', :text => 'See https://gitlab.com/obsly/obsly.git' }
  s.summary  = 'Obsly:a mobile remote logger'
  s.description = 'mobile development easier with Obsly.'
  s.homepage = 'https://gitlab.com/obsly/obsly'
  s.author   = { 'Obsly' => 'joaquinc@sfy.com' }
  s.requires_arc = true
  s.source   = {
    :git => 'https://gitlab.com/obsly/obsly.git',
    :tag => s.version.to_s
  }
  s.swift_versions = ['5.5']
  s.frameworks = 'Foundation', 'SystemConfiguration', 'Security', 'MobileCoreServices'
  s.library = 'c++'
  s.vendored_frameworks = 'ObslySDK.xcframework'
  s.cocoapods_version = '>= 1.11.3'
  s.pod_target_xcconfig = { 'VALID_ARCHS' => 'arm64 x86_64' }
  
  s.preserve_path = ['utils/symbols-uploader']
  
end

