//  ProcessStartTime.h
//  Obsly

#ifndef ProcessStartTime_h
#define ProcessStartTime_h

#include <stdio.h>
#import <QuartzCore/QuartzCore.h>

#endif /* ProcessStartTime_h */

CFTimeInterval processStartTime(void);
int64_t getFreeMemory(void);
