// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.9 (swiftlang-5.9.0.128.108 clang-1500.0.40.1)
// swift-module-flags: -target arm64-apple-ios14.1-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Osize -module-name ObslySDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Combine
import CommonCrypto
import CoreTelephony
import DeveloperToolsSupport
import Foundation
import JavaScriptCore
import MachO
import Network
@_exported import ObslySDK
import PDFKit
import PDFKit.PDFView
import Swift
import SwiftUI
import SystemConfiguration
import UIKit.UIDevice
import UIKit
import WebKit.WKWebView
import WebKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
import os.log
import os
@_documentation(visibility: internal) extension UIKit.UIDeviceOrientation : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
@_hasMissingDesignatedInitializers final public class Step {
  final public func finish(updatedDescription: Swift.String? = nil)
  @objc deinit
}
extension ObslySDK.Obsly {
  public enum Metrics {
    public enum MetricResult {
      case success, error
      public static func == (a: ObslySDK.Obsly.Metrics.MetricResult, b: ObslySDK.Obsly.Metrics.MetricResult) -> Swift.Bool
      public func hash(into hasher: inout Swift.Hasher)
      public var hashValue: Swift.Int {
        get
      }
    }
    public static var defaultTimeout: Swift.Double {
      get
    }
    public static func startMetric(vc: UIKit.UIViewController, processName: Swift.String, operation: Swift.String? = nil, viewName: Swift.String? = nil, fbl: Swift.String? = nil, timeout: Swift.Double = defaultTimeout)
    public static func finishPageLoadComplete(vc: UIKit.UIViewController, result: ObslySDK.Obsly.Metrics.MetricResult)
    public static func finishMetric(vc: UIKit.UIViewController, processName: Swift.String, operation: Swift.String? = nil, viewName: Swift.String? = nil, fbl: Swift.String? = nil, result: ObslySDK.Obsly.Metrics.MetricResult)
    public static func setCurrent(fbl: Swift.String)
    public static func setCurrent(operation: Swift.String)
    public static func clearCurrentFbl()
    public static func clearCurrentOperation()
  }
}
@_hasMissingDesignatedInitializers final public class Transaction {
  final public func startStep(name: Swift.String, description: Swift.String? = nil, startTime: ObslySDK.TimeToken? = nil) -> ObslySDK.Step
  final public func finish(updatedDescription: Swift.String? = nil)
  @objc deinit
}
public enum LoggerLevel : Swift.UInt8, Swift.Comparable {
  case trace, debug, info, warning, error, none
  public static func < (lhs: ObslySDK.LoggerLevel, rhs: ObslySDK.LoggerLevel) -> Swift.Bool
  public init?(rawValue: Swift.UInt8)
  public typealias RawValue = Swift.UInt8
  public var rawValue: Swift.UInt8 {
    get
  }
}
public struct ObslyOptions {
  public var isCrasherEnabled: Swift.Bool
  public var isLifeCycleEnabled: Swift.Bool
  public var isRequestEnabled: Swift.Bool
  public var isTaggerEnabled: Swift.Bool
  public var isAutoTagsEnabled: Swift.Bool
  @available(*, deprecated, message: "This variable is unused and will be removed, use capture RequestCaptureModel")
  public var captureResponseBodyOnError: Swift.Bool
  public var isViewHierarchyEnabled: Swift.Bool
  public var isMetricsEnabled: Swift.Bool
  public var hostBlacklist: [Swift.String]
  public var autoTaggerConsumer: ObslySDK.RulesOutputConsumer?
  public var debugMode: Swift.Bool {
    get
    set
  }
}
public struct RequestCaptureModel {
  public init(url: Swift.String, captureCodesRange: Swift.ClosedRange<Swift.Int>, captureResponseBody: Swift.Bool, captureRequestBody: Swift.Bool)
}
extension ObslySDK.Obsly {
  public struct Performance {
    @discardableResult
    public func startTransaction(name: Swift.String, description: Swift.String? = nil, startTime: ObslySDK.Obsly.Performance.TimeToken? = nil, autofinishWithStepsCount: Swift.Int? = nil) -> ObslySDK.Transaction
    public func finishTransaction(name: Swift.String, updatedDescription: Swift.String? = nil) throws
    @discardableResult
    public func startStep(name: Swift.String, transaction: Swift.String, description: Swift.String? = nil, startTime: ObslySDK.Obsly.Performance.TimeToken? = nil) throws -> ObslySDK.Step
    public func finishStep(name: Swift.String, transaction: Swift.String, updatedDescription: Swift.String? = nil) throws
    @discardableResult
    public static func startTransaction(name: Swift.String, description: Swift.String? = nil, startTime: ObslySDK.Obsly.Performance.TimeToken? = nil, autofinishWithStepsCount: Swift.Int? = nil) -> ObslySDK.Transaction?
    public static func finishTransaction(name: Swift.String, updatedDescription: Swift.String? = nil) throws
    @discardableResult
    public static func startStep(name: Swift.String, transaction: Swift.String, description: Swift.String? = nil, startTime: ObslySDK.Obsly.Performance.TimeToken? = nil) throws -> ObslySDK.Step?
    public static func finishStep(name: Swift.String, transaction: Swift.String, updatedDescription: Swift.String? = nil) throws
    public enum Error : Swift.String, Swift.Error {
      case obslyNotStarted
      case transactionsDoesNotExist
      case stepDoesNotExist
      public init?(rawValue: Swift.String)
      public typealias RawValue = Swift.String
      public var rawValue: Swift.String {
        get
      }
    }
  }
}
extension ObslySDK.Obsly.Performance {
  public struct TimeToken {
    public static func now() -> ObslySDK.Obsly.Performance.TimeToken
  }
}
public typealias TimeToken = ObslySDK.Obsly.Performance.TimeToken
@_documentation(visibility: internal) public enum BinaryEventTypeEnum : Swift.String, Swift.Codable, Swift.CaseIterable {
  case photo
  case video
  public init?(rawValue: Swift.String)
  public typealias AllCases = [ObslySDK.BinaryEventTypeEnum]
  public typealias RawValue = Swift.String
  public static var allCases: [ObslySDK.BinaryEventTypeEnum] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
@_documentation(visibility: internal) public struct BinaryEvent : Swift.Codable {
  public init(path: Swift.String, name: Swift.String? = nil, type: ObslySDK.BinaryEventTypeEnum)
  public init(from decoder: any Swift.Decoder) throws
  public func encode(to encoder: any Swift.Encoder) throws
}
@_documentation(visibility: internal) public protocol ObslyProducer : AnyObject {
  var name: Swift.String { get }
  init(registerEvent: @escaping ((ObslySDK.BinaryEvent) -> Swift.Void))
}
final public class Obsly {
  final public let performance: ObslySDK.Obsly.Performance
  public static var defaultURL: Foundation.URL {
    get
  }
  final public func flush(timeout: Swift.Double, completion: ((Swift.Bool) -> Swift.Void)?)
  public static func register(producer: any ObslySDK.ObslyProducer.Type)
  public init(apiKey: Swift.String, instanceURL: Foundation.URL = Obsly.defaultURL, pinnedHosts: [Swift.String : [Swift.String]] = [:], _ closure: (inout ObslySDK.ObslyOptions) -> Swift.Void)
  final public func start()
  final public func add(tags: [Swift.String : Swift.String]?, category: Swift.String?)
  final public func set(environment: Swift.String)
  final public func set(release: Swift.String)
  final public func set(userID: Swift.String?)
  final public func set(personID: Swift.String?)
  final public func set(passportID: Swift.String?)
  final public func set(contractID: Swift.String?)
  final public func set(appName: Swift.String?)
  final public func setRequestBlacklist(hosts: [Swift.String])
  final public func setRequestCapture(models: [ObslySDK.RequestCaptureModel])
  final public func updateOptions(_ closure: (inout ObslySDK.ObslyOptions) -> Swift.Void)
  final public func setRuleParams(scope: ObslySDK.Obsly.Scope, params: [Swift.String : Swift.String])
  final public func removeRuleParams(scope: ObslySDK.Obsly.Scope, keys: [Swift.String])
  final public func createNewSession(name: Swift.String?)
  final public func closeCurrentSession()
  final public func getObslyUUID() -> Swift.String
  public static let newTranslationsNotificationKey: Foundation.NSNotification.Name
  final public func getTranslation(languageCode: Swift.String, key: Swift.String) -> Swift.String?
  @_documentation(visibility: internal) final public func addExtraBinaryEvent(path: Swift.String, name: Swift.String, type: Swift.String)
  final public func setRequestHeadersWhitelist(headers: [Swift.String])
  @objc deinit
}
extension ObslySDK.Obsly {
  public enum Scope : Swift.String, Swift.CaseIterable {
    case execution
    case session
    public init?(rawValue: Swift.String)
    public typealias AllCases = [ObslySDK.Obsly.Scope]
    public typealias RawValue = Swift.String
    public static var allCases: [ObslySDK.Obsly.Scope] {
      get
    }
    public var rawValue: Swift.String {
      get
    }
  }
}
public typealias RulesOutputConsumer = (ObslySDK.RuleOutput) -> Swift.Void
public struct RuleOutput {
  public let tags: [Swift.String : Any]
  public let category: Swift.String?
  public let type: Swift.String?
}
extension ObslySDK.Obsly.Metrics.MetricResult : Swift.Equatable {}
extension ObslySDK.Obsly.Metrics.MetricResult : Swift.Hashable {}
extension ObslySDK.LoggerLevel : Swift.Hashable {}
extension ObslySDK.LoggerLevel : Swift.RawRepresentable {}
extension ObslySDK.Obsly.Performance.Error : Swift.Equatable {}
extension ObslySDK.Obsly.Performance.Error : Swift.Hashable {}
extension ObslySDK.Obsly.Performance.Error : Swift.RawRepresentable {}
extension ObslySDK.BinaryEventTypeEnum : Swift.Equatable {}
extension ObslySDK.BinaryEventTypeEnum : Swift.Hashable {}
extension ObslySDK.BinaryEventTypeEnum : Swift.RawRepresentable {}
extension ObslySDK.Obsly.Scope : Swift.Equatable {}
extension ObslySDK.Obsly.Scope : Swift.Hashable {}
extension ObslySDK.Obsly.Scope : Swift.RawRepresentable {}
